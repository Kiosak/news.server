const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
var cookieSession = require('cookie-session');

import connectDB from './db/index';

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();
require('dotenv').config();

app.use(
  cookieSession({
    name: 'DB-session',
    keys: ['WeH4zcgTSf6D39KqgB0IVgH3455wNbpr', 'TiPPTtb7HB9BLFMoMETiSgAnR9JEn7Pq'],
    secure: false,
  })
);

var cors = require('cors');
app.use(
  cors({
    origin: process.env.HOST,
    credentials: true,
  })
);

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'X-Requested-With, Content-type,Accept,X-Access-Token,X-Key,token, Authorization'
  );
  res.header('Access-Control-Allow-Origin', process.env.HOST);
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  return next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Connectin to Data Base
connectDB();

module.exports = app;
