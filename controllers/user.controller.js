import User from '../models/user';

const register = async (req, res) => {
  const user = req.body.user;
  if (!user.email) {
    return res.status(422).send({ success: false, message: 'email is required' });
  }
  if (!user.password) {
    return res.status(422).send({ success: false, message: 'password is required' });
  }
  if (!user.firstname || !user.lastname) {
    return res.status(422).send({ success: false, message: 'First and Last Names are required' });
  }

  if ((await User.findOne({ email: user.email })) !== null) {
    return res.status(422).send({ success: false, message: 'User already exist' });
  }

  const newUser = new User(user);
  newUser.setPassword(user.password);
  newUser.save().then(
    userInDB => {
      req.session.user = userInDB._id;
      return res
        .status(200)
        .send({ success: true, message: 'logged in', user: userInDB.toAuthJSON() });
    },
    error => {
      return res.status(422).send({ success: false, message: 'could not save user', error });
    }
  );
};

const login = (req, res) => {
  const user = req.body.user;
  if (!user) {
    return res.status(422).send({ success: false, message: 'POST body required' });
  }
  if (!user.email) {
    return res.status(422).send({ success: false, message: 'email required' });
  }
  if (!user.password) {
    return res.status(422).send({ success: false, message: 'password required' });
  }

  User.findOne({ email: user.email }).then(userInDB => {
    if (!userInDB) {
      return res.status(404).send({ success: false, message: 'Invalid email or password' });
    }
    if (!userInDB.validatePassword(user.password)) {
      return res.status(404).send({ success: false, message: 'Invalid email or password' });
    }
    req.session.user = userInDB._id;
    return res.status(200).send({
      success: true,
      message: 'login accepted',
      user: userInDB.toAuthJSON(),
    });
  });
};

module.exports = {
  register,
  login,
};
