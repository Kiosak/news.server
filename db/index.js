const mongoose = require('mongoose');
require('dotenv').config();

const connectDB = () => {
  const uri = `mongodb://${process.env.MONGO_URI}:${process.env.MONGO_PORT}/testdb`;
  return mongoose.connect(
    uri,
    {
      useNewUrlParser: true,
    },
    err => {
      if (err) {
        console.log('mongo err :', err);
      } else {
        console.log('mongoDB connected!');
      }
    }
  );
};

export default connectDB;
