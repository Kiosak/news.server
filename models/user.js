const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    hash: String,
    salt: String,
    refreshToken: String,
  },
  {
    timestamps: {
      createdAt: 'created',
      updatedAt: 'updated',
    },
  }
);

userSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

userSchema.methods.validatePassword = function(password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

userSchema.methods.generateJWT = function() {
  const today = new Date();

  const tokenExpiration = new Date(today);
  tokenExpiration.setMinutes(today.getMinutes() + 30);
  const refreshTokenExpiration = new Date(today);
  refreshTokenExpiration.setMinutes(today.getMinutes() + 60 * 30 * 24);

  const refreshToken = jwt.sign(
    {
      email: this.email,
      id: this._id,
      exp: parseInt(refreshTokenExpiration / 1000, 10),
    },
    'CgdIOsXaBWQVA4Oq2t5nLIKvwGPx1VhP'
  );
  this.refreshToken = refreshToken;
  this.save();

  return {
    token: jwt.sign(
      {
        email: this.email,
        id: this._id,
        exp: parseInt(tokenExpiration / 1000, 10),
      },
      'CgdIOsXaBWQVA4Oq2t5nLIKvwGPx1VhP'
    ),
    expirity: tokenExpiration,
    refreshToken,
  };
};

userSchema.methods.toAuthJSON = function() {
  const authentication = this.generateJWT();
  return {
    id: this._id,
    firstname: this.firstname,
    lastname: this.lastname,
    email: this.email,
    token: authentication.token,
    expirity: authentication.expirity,
    refreshToken: authentication.refreshToken,
  };
};

userSchema.set('toJSON', {
  virtuals: true,
  transform: function(doc, ret, opt) {
    delete ret['hash'];
    delete ret['salt'];
    delete ret['refreshToken'];
    return ret;
  },
});

const User = mongoose.model('User', userSchema);

export default User;
